//Koi Fish Bot Version 1

package KoiFishBot.KoiFishBot;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
//import net.dv8tion.jda.core.entities.PrivateChannel;
//import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.Random;

public class KoiBot extends ListenerAdapter
{
    public static void main( String[] args ) throws LoginException, IllegalArgumentException, InterruptedException, RateLimitedException
    {
        JDA jdaBot = new JDABuilder(AccountType.BOT).setToken("REMOVED-PRIVATE").buildBlocking();
        jdaBot.addEventListener(new KoiBot());
    }
    
    @Override
    public void onMessageReceived(MessageReceivedEvent event)
    {
    	Message objMsg = event.getMessage();
    	MessageChannel objChannel = event.getChannel();
    	//User objUser = event.getAuthor();
    	//PrivateChannel privateChannel = event.getPrivateChannel();
		
    	//Autoresponder messages
    	/*
    	if(objMsg.getContent().equals("test"))
    	{
    		objChannel.sendMessage("testing 123 " + objUser.getAsMention()).queue();
    	}
    	*/
    	
    	//Choose what starts a command
    	char commandInit = '>';
    	
    	
    	//Commands
    	if(objMsg.getContent().charAt(0) == commandInit)
    	{
    		String[] strArgs = objMsg.getContent().substring(1).split(" ");
    		
    		//check ping
    		if(strArgs[0].equalsIgnoreCase("ping"))
        	{
    			
        		objChannel.sendMessage("Pong").queue();
        	}
    		//end ping
    		
    		//check if pal
    		if (strArgs[0].equalsIgnoreCase("pal"))
    		{
    			//BEGIN CONVERTING TO LOWER CASE
    			String strPal = strArgs[1];
    			char letter;
    			int length;
    			String result = "";
    			
    			length = strPal.length();
    			
    			for(int spot=0;spot<length;spot++)
    			{
    				letter = strPal.charAt(spot);
	    			if((letter>='A')&&(letter<='Z'))
	    			{
	    				result = result + (char)((int)letter-(int)('A')+(int)('a'));
	    			}
	    			else if((letter>='a')&&(letter<='z'))
	    			{
	    				result = result + letter;
	    			}
	    			else
	    			{
	    				//ignore
	    			}
    			}
    			//END CONVERTING TO LOWER CASE
    			
    			//BEGIN CHECKING IF PAL
    			boolean blnPal = true;
    			for (int i = 0; i < result.length(); i++)
    			{
    				if (result.charAt(i) != result.charAt(result.length() - 1 - i))
    				{
    					blnPal = false;
    				}
    			}
    			if(blnPal==true)
    			{
    				objChannel.sendMessage(strPal + " is a palindrome").queue();
    			}
    			else
    			{
    				objChannel.sendMessage(strPal + " is NOT a palindrome").queue();
    			}
    			//END CHECKING IF PAL
    		}
    		//end pal
    		
    		//rock paper scissors
    		if (strArgs[0].equalsIgnoreCase("rps"))
    		{
    			Random randomizer = new Random();
    			String rps = strArgs[1];
    			String choices[] = new String[3];
    			choices[0] = "Rock";
    			choices[1] = "Paper";
    			choices[2] = "Scissors";
    			
    			if(rps.equals(null))
    			{
    				objChannel.sendMessage("You didn't choose 'rock', 'paper', or 'scissors'").queue();
    			}
    			
    			if(rps.equalsIgnoreCase(choices[0]))
    			{
        			int index = randomizer.nextInt(3);
        			objChannel.sendMessage("You chose: **"+ choices[0] +"**. I chose: **"+choices[index]+"**.").queue();
        			if(rps.equalsIgnoreCase(choices[index]))
        			{
        				objChannel.sendMessage("It's a draw!").queue();
        			}
        			else if(choices[index].equalsIgnoreCase(choices[1]))
        			{
        				objChannel.sendMessage("Paper beats rock, I win!").queue();
        			}
        			else if(choices[index].equalsIgnoreCase(choices[2]))
        			{
        				objChannel.sendMessage("Rock beats scissors, you win!").queue();
        			}
    			}
    			else if(rps.equalsIgnoreCase(choices[1]))
    			{
        			int index = randomizer.nextInt(3);
        			objChannel.sendMessage("You chose: **"+ choices[1] +"**. I chose: **"+choices[index]+"**.").queue();
        			if(rps.equalsIgnoreCase(choices[index]))
        			{
        				objChannel.sendMessage("It's a draw!").queue();
        			}
        			else if(choices[index].equalsIgnoreCase(choices[0]))
        			{
        				objChannel.sendMessage("Paper beats rock, you win!").queue();
        			}
        			else if(choices[index].equalsIgnoreCase(choices[2]))
        			{
        				objChannel.sendMessage("Scissors beats paper, I win!").queue();
        			}
    			}
    			else if(rps.equalsIgnoreCase(choices[2]))
    			{
        			int index = randomizer.nextInt(3);
        			objChannel.sendMessage("You chose: **"+ choices[2] +"**. I chose: **"+choices[index]+"**.").queue();
        			if(rps.equalsIgnoreCase(choices[index]))
        			{
        				objChannel.sendMessage("It's a draw!").queue();
        			}
        			else if(choices[index].equalsIgnoreCase(choices[0]))
        			{
        				objChannel.sendMessage("Rock beats scissors, I win!").queue();
        			}
        			else if(choices[index].equalsIgnoreCase(choices[1]))
        			{
        				objChannel.sendMessage("Scissors beats paper, you win!").queue();
        			}
    			}
    			else
    			{
    				objChannel.sendMessage("You didn't choose 'rock', 'paper', or 'scissors'.").queue();
    			}
    		}
    		//end rps
    		
    		//8ball
    		if (strArgs[0].equalsIgnoreCase("8ball")&&strArgs[1]!=null) //temporary, check if second word is a thing
    		{
    			Random randomizer = new Random();
    			String outcomes[] = new String[10];
    			outcomes[0] = "As I see it, yes";
    			outcomes[1] = "Better not tell you now";
    			outcomes[2] = "Don't count on it";
    			outcomes[3] = "In your dreams";
    			outcomes[4] = "It is certain";
    			outcomes[5] = "Most likely";
    			outcomes[6] = "Signs point to yes";
    			outcomes[7] = "Very doubtful";
    			outcomes[8] = "Without a doubt";
    			outcomes[9] = "Yes, definitely";
    			int index = randomizer.nextInt(10);
    			objChannel.sendMessage(outcomes[index]).queue();
    		}
    		//end 8ball
    		
    		//help
    		if (strArgs[0].equalsIgnoreCase("help"))
    		{
    			objChannel.sendMessage(
    					"Koi Fish Bot is currently in development.\n"
    					+ "**List of commands:**\n"
    					+ "`>ping` - test bot delay\n"
    					+ "`>pal [word]` - check if word is a palindrome\n"
    					+ "`>rps [rock, paper, scissors]` - play rock paper scissors with the bot\n"
    					+ "`>8ball [yes or no question]` - test your luck\n"
    					).queue();
    		}
    		//end help
    		

    	}
    }
}
